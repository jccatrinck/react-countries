import { loader } from 'graphql.macro';

export const QUERY_COUNTRIES = loader('./queries/countries.gql');
export const QUERY_COUNTRY = loader('./queries/country.gql');
export const QUERY_COUNTRIES_NAMES_IN = loader('./queries/countries_names_in.gql');

export const MUTATE_COUNTRY = loader('./mutations/country.gql');

export default loader('./schema/schema.gql')