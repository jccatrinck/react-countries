import React from 'react';
import css from '@emotion/css/macro';
import styled from '@emotion/styled/macro';
import { Link } from 'react-router-dom';

import { unit } from '../../styles';

const padding = 2 * unit;

const StyledLink = styled(Link)`
  position: relative;
  display: block;
  height: 235px;
  margin-top: ${padding}px;
  text-decoration: none;
  border: 1px solid silver;
  border-radius: 7px;
  color: white;
  padding: ${4 * unit}px ${5 * unit}px;
  &:not(:last-child) {
    margin-bottom: ${2 * padding}px;
  };
`;

const ClassCover = css`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

const StyleDivWrapper = styled.div(ClassCover, `
background: black;
border-radius: 7px;
opacity: 1;
z-index: -1;
`);

const WrappedBackground = ({ background, ...props }) => (
  <StyleDivWrapper>
    <div style={{ backgroundImage: `url(${background})` }} {...props} />
  </StyleDivWrapper>
)

const StyledImage = styled(WrappedBackground)(ClassCover, `
background: white;
border-radius: 7px;
opacity: 0.9;
background-size: cover;
background-position: center;
`);

const ClassHeader = css`
  opacity: 1;
  text-shadow: 0 0 5px #000;
`

const StyledTitle = styled.h2(ClassHeader)
const StyledSubTitle = styled.h4(ClassHeader)

const Card = ({ title, subtitle, background, link }, ref) => {
  return (
    <StyledLink
      ref={ref}
      to={link}
    >
      <StyledImage background={background} />
      <StyledTitle>{title}</StyledTitle>
      <StyledSubTitle>{subtitle}</StyledSubTitle>
    </StyledLink>
  );
}

export default React.forwardRef(Card);
