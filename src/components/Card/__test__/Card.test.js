import React from 'react';
import ReactDOM from 'react-dom';
import Card from '../Card';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import renderer from 'react-test-renderer';
import { MemoryRouter } from "react-router";

const CardWithRoute = (props) => (
  <MemoryRouter>
    <Card link='/' {...props} />
  </MemoryRouter>
)

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CardWithRoute />, div);
});

it('renders correctly', () => {
  const content = 'content';
  const { getByText } = render(<CardWithRoute title={content} />);

  expect(getByText(content)).not.toBeEmpty();
});

it('has correct props', () => {
  const background = 'data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg"/>';

  const root = renderer
    .create(
      <CardWithRoute background={background} />
    )
    .root;

  const hasSome = root.children.some(item => item.findByProps({background}))

  expect(hasSome).toBeTruthy()
});