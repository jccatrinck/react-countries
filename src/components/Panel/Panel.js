import styled from '@emotion/styled';

const Panel = styled.div`
  font-size: 22px;
  background: rgba(100, 100, 100, 0.25);
  width: 100%;
  padding: 15px;
  border: 5px solid rgba(100, 100, 100, 1);
  border-radius: 7px;
  overflow: auto;
  overflow-y: hidden;
`

export default Panel
