import React from 'react';
import ReactDOM from 'react-dom';
import Map from '../Map';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

const dimensions = {
  width: 400,
  height: 400
}

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Map dimensions={dimensions} />, div);
});

it('renders correctly', () => {
  const { container } = render(<Map dimensions={dimensions} />);

  expect(container).not.toBeUndefined();
});