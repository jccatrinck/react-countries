import React, { useRef } from 'react'
import { getDataURL } from '../../utils/imagery'
import { encodeQueryString } from '../../utils/uri'
import map from '../../utils/map'
import styled from '@emotion/styled';

const StyledImg = styled.img`
  display: block;
  margin: 0 auto;
`;

const Map = (props) => {
  const params = map.getDefaultParams()

  params.push(
    {
      key: 'size',
      value: `${props.dimensions.width}x${props.dimensions.height}`
    }
  )

  if (props.routes) {
    params.push(
      map.createMarkers('red', props.routes[0].from.name, [
        props.routes[0].from.coords
      ])
    )
  
    props.routes.forEach((route, i) => {
      params.push(
        map.createPath('black', 3, {
          from: route.from.coords,
          to: route.to.coords
        })
      )
  
      params.push(
        map.createMarkers('red', i+1, [
          route.to.coords
        ])
      )
    })
  }

  const queryString = encodeQueryString(params)

  let url = `http://maps.google.com/maps/api/staticmap?${queryString}`

  // Cache
  const imgRef = useRef()

  const dataURI = localStorage.getItem(encodeURIComponent(url))

  if (dataURI) {
    url = dataURI
  }

  const handleError = (e) => {
    if (e.target) {
      if (url.protocol !== 'data:') {
        localStorage.removeItem(encodeURIComponent(e.target.src))
      }
    }
  }

  const handlerLoad = () => {
    const { current: img } = imgRef

    if (img) {
      const url = new URL(img.src);

      if (url.protocol !== 'data:') {
        const dataURI = getDataURL(img)

        if (dataURI) {
          localStorage.setItem(encodeURIComponent(img.src), dataURI)
        }
      }
    }
  }

  return (
    <StyledImg ref={imgRef} alt="map" crossOrigin="anonymous" src={url} onLoad={handlerLoad} onError={handleError} />
  )
}

export default Map
