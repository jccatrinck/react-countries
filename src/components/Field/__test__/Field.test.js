import React from 'react';
import ReactDOM from 'react-dom';
import Field from '../Field';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Field />, div);
});

it('renders correctly', () => {
  const { getByRole } = render(
    <Field type="text" />
  );

  const input = getByRole('textbox');
  
  expect(input).toHaveAttribute('type', 'text');
});