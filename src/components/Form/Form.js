import React, { useState } from 'react';
import { useMutation } from '@apollo/client';
import { MUTATE_COUNTRY } from '../../gql';
import Button from '../Button/Button';
import Field from '../Field/Field';

const Form = ({ country }) => {  
  const [ mutate, { data } ] = useMutation(MUTATE_COUNTRY);

  const [capital, setCapital] = useState(country.capital);
  
  const handleCapital = (e) => {
    setCapital(e.target.value);
  };

  const [area, setArea] = useState(country.area);
  
  const handleArea = (e) => {
    setArea(e.target.value);
  };

  const [population, setPopulation] = useState(country.population);
  
  const handlePopulation = (e) => {
    setPopulation(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    mutate({
      variables: {
        country: {
          name: country.name,
          capital,
          area,
          population
        }
      } 
    });
  };

  let msg;

  if (data) {
    msg = <p>Successfully changed!</p>
  }

  const focus = (el) => el && el.focus();

  return (
    <form onSubmit={handleSubmit}>
      <h2>{country.name}</h2>
      <hr />
      {msg}
      <Field
        ref={focus}
        label="Capital"
        type="text"
        name="capital"
        value={capital}
        onChange={handleCapital}
      />
      <Field
        label="Area"
        type="text"
        name="area"
        value={area}
        onChange={handleArea}
      />
      <Field
        label="Population"
        type="text"
        name="population"
        value={population}
        onChange={handlePopulation}
      />
      <Button type="submit" float="right">
        Save
      </Button>
    </form>
  );
};

export default Form;
