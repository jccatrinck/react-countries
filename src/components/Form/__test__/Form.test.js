import React from 'react';
import ReactDOM from 'react-dom';
import Form from '../Form';
import '@testing-library/jest-dom/extend-expect';
import { MockedProvider } from '@apollo/client/testing';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <MockedProvider mocks={[]}>
      <Form country={{}} />
    </MockedProvider>,
    div)
  ;
});