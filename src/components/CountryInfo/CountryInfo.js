import React from 'react';

const CountryInfo = (props) => {
  const description = [];

  if (props.name) {
    description.push(`Name: ${props.name}`)
  }

  if (props.capital) {
    description.push(`Capital: ${props.capital}`)
  }

  if (props.area) {
    const area = props.area.toLocaleString();
    description.push(`Area: ${area} km²`)
  }

  if (props.population) {
    const population = props.population.toLocaleString();
    description.push(`Population: ${population}`)
  }

  if (props.topLevelDomains) {
    const topLevelDomains = props.topLevelDomains.map(domain => domain.name).join(', ');    
    description.push(`Top-level domains: (${topLevelDomains})`)
  }

  let nearest;

  if (props.distanceToOtherCountries) {
    const countries = props.distanceToOtherCountries.map((item, i) => {
      let nearCountry = `${i+1} - ${item.countryName}`
  
      if (item.distanceInKm) {
        const distanceInKm = item.distanceInKm.toLocaleString();
  
        nearCountry = `${nearCountry} - ${distanceInKm} km`
      }
      
      return nearCountry;
    }).join("\n")

    nearest = (
      <>
        {"\n\n"}
        Top 5 nearest countries:{"\n"}
        {countries}
      </>
    )
  }

  let img;

  if (props.flag && props.flag.svgFile) {
    img = <img alt="flag" src={props.flag.svgFile} height="50px" />
  }

  return (
    <>
      {img}
      <pre>
        {description.join("\n")}   
        {nearest}     
      </pre>
      {props.children && <div>{props.children}</div>}
    </>
  )
}

export default CountryInfo
