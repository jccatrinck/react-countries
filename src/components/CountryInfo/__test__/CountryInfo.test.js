import React from 'react';
import ReactDOM from 'react-dom';
import CountryInfo from '../CountryInfo';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CountryInfo />, div);
});

it('renders correctly', () => {
  const props = {
    name: 'name',
    capital: 'capital',
    area: 'area',
    population: 'population'
  };

  const { container } = render(<CountryInfo {...props} />);

  const nodes = container.getElementsByTagName('*');

  const children = [...nodes];

  const name = children.some(node => node.textContent.includes(props.name))
  const capital = children.some(node => node.textContent.includes(props.capital))
  const area = children.some(node => node.textContent.includes(props.area))
  const population = children.some(node => node.textContent.includes(props.population))

  expect(name).toBeTruthy();
  expect(capital).toBeTruthy();
  expect(area).toBeTruthy();
  expect(population).toBeTruthy();
});

it('matches snapshot', () => {
  const tree = renderer
    .create(
      <CountryInfo name="test" />
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});