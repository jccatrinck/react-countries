import React from 'react';
import { useHistory } from 'react-router-dom';
import Button from '../Button/Button';

const BackButton = (props) => {
  const history = useHistory()

  return (
    <Button data-testid="back-button" onClick={() => history.goBack()}>{ props.children || 'Back' }</Button>
  )
}

export default BackButton
