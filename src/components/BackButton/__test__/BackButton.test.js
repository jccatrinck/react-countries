import React from 'react';
import ReactDOM from 'react-dom';
import BackButton from '../BackButton'
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<BackButton />, div)
})

it('renders correctly', () => {
  const content = 'content'
  const { getByText } = render(<BackButton>{content}</BackButton>)
  expect(getByText(content)).not.toBeEmpty()
})