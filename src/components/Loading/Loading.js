import React from 'react'
import styled from '@emotion/styled';
import { animations } from '../../styles';

const StyledDiv = styled.div`
  display: block;
  width: 100%;
  height: 80px;
  margin: 0 8px;
  &:after {
    content: " ";
    display: block;
    width: 64px;
    height: 64px;
    margin: 8px auto;
    border-radius: 50%;
    border: 6px solid #000;
    border-color: #000 transparent #000 transparent;
    animation: ${animations.rotate} 1.2s linear infinite;
  }
`;

const Loading = (props) => (
  <StyledDiv {...props} />
)

export default Loading
