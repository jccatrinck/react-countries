import React from 'react';
import ReactDOM from 'react-dom';
import Loading from '../Loading';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import renderer from 'react-test-renderer';
import { matchers } from 'jest-emotion';

expect.extend(matchers)

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Loading />, div);
});

it('renders correctly', () => {
  const { container } = render(<Loading />);

  expect(container).toBeVisible();
});

it('floats', () => {
  const tree = renderer
    .create(
      <Loading />
    )
    .toJSON();

  expect(tree).toHaveStyleRule('display', 'block');
});