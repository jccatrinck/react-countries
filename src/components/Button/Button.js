import React from 'react';
import { Link } from 'react-router-dom';
import styled from '@emotion/styled/macro';
import css from '@emotion/css/macro';

const ClassStyle = css`
  cursor: pointer;
  appearance: button;
  align-items: flex-start;
  background-color: rgb(239, 239, 239);
  border-width: 2px;
  border-style: outset;
  border-color: rgb(195, 195, 195);
  border-image: initial;
  margin: 5px;
  font-size: 16px;
  padding: 6px 10px;
  border-radius: 8px;  
`;

const float = ({ float }) => float ? css({float}) : '';

const StyledButton = styled.button`
  ${ClassStyle}
  ${float}
`;

const StyledLink = styled(Link)`
  ${ClassStyle}
  ${float}
  text-decoration: none;
  &:visited {
    color: black;
  }
`;

const Button = (props, ref) => {
  let Tag = StyledButton

  if (props.to) {
    Tag = StyledLink;
  }

  return (
    <Tag ref={ref} data-testid="button" {...props} />
  );
}

export default React.forwardRef(Button);
