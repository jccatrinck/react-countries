import React from 'react';
import ReactDOM from 'react-dom';
import Button from '../Button';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import renderer from 'react-test-renderer';
import { matchers } from 'jest-emotion';

expect.extend(matchers)

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Button />, div);
});

it('renders correctly', () => {
  const content = 'content';
  const { getByText } = render(<Button>{content}</Button>);

  expect(getByText(content)).not.toBeEmpty();
});

it('floats', () => {
  const tree = renderer
    .create(
      <Button float="left" />
    )
    .toJSON()

  expect(tree).toHaveStyleRule('float', 'left')
});