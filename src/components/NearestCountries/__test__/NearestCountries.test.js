import React from 'react';
import ReactDOM from 'react-dom';
import NearestCountries from '../NearestCountries';
import '@testing-library/jest-dom/extend-expect';
import { MockedProvider } from '@apollo/client/testing';
import { QUERY_COUNTRIES_NAMES_IN } from '../../../gql';

const mocks = [
  {
    request: {
      query: QUERY_COUNTRIES_NAMES_IN,
      variables: {
        names: ['Brazil'],
      },
    },
    result: {
      data: {
        countries: { name: 'Brazil', location: { longitude: -55, latitude: -10 }},
      },
    },
  },
];

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <NearestCountries />
    </MockedProvider>,
    div
  );
});