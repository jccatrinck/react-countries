import React, { useState, useCallback } from 'react';
import { useQuery } from '@apollo/client';
import styled from '@emotion/styled';
import { QUERY_COUNTRIES_NAMES_IN } from '../../gql';
import Loading from '../Loading/Loading';
import Map from '../Map/Map';

const StyledDiv = styled.div`
  min-height: 400px;
`;

const NearestCountries = (props) => {
  const [dimensions, setDimensions] = useState(null);

  const { origin, names } = props;

  const { loading, error, data }  = useQuery(QUERY_COUNTRIES_NAMES_IN, {
    variables: { names }
  });

  let routes;

  if (data && data.countries && dimensions) {
    const countries = data.countries.slice(0);
    
    countries.sort(function(a, b){  
      return names.indexOf(a.name) - names.indexOf(b.name);
    });

    routes = countries.map((destiny) => ({
      from: {
        name: origin.name,
        coords: [origin.location.latitude, origin.location.longitude]
      },
      to: {
        name: destiny.name,
        coords: [destiny.location.latitude, destiny.location.longitude]
      }
    }));
  }

  const divRef = useCallback((div) => {
    if (div) {
      const { width, height } = div.getBoundingClientRect();
      setDimensions({ width, height });
    }    
  }, []);

  let map;

  if (dimensions && routes) {
    map = <Map dimensions={dimensions} routes={routes} />
  }

  return (
    <StyledDiv ref={divRef}>
      {map}
      {error && <p>Error :(</p>}
      {loading && <Loading />}
    </StyledDiv>
  )
}

export default NearestCountries;
