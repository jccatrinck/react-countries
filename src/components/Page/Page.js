import React from 'react';
import styled from '@emotion/styled';

import { unit } from '../../styles';

const Container = styled('div')`
    width: 100%;
    max-width: 1200px;
    margin: 0 auto;
    padding: ${2 * unit}px;
    padding-bottom: ${5 * unit}px;
}
`;

const Page = (props) => {
  return (
    <Container>{props.children}</Container>
  );
};

export default Page;
