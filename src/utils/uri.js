export const encodeQueryString = (params) => params.map(({ key, value }) => {
  return  [key, value].filter(Boolean).join('=')
}).join('&')