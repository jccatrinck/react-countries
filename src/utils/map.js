const encodeMapParamsComponent = (value) => value.map(({key, value}) => {
  return [key, value].filter(Boolean).join(':')
}).join('|');

const DEFAULT_PARAMS = [
  {
    key: 'key',
    value: process.env.REACT_APP_GOOGLE_STATIC_MAPS_KEY
  },
  {
    key: 'style',
    value: encodeMapParamsComponent([
      {
        key: 'feature',
        value: 'poi'
      },
      {
        key: 'visibility',
        value: 'off'
      }
    ])
  },
  {
    key: 'sensor',
    value: 'false'
  },
]

const getDefaultParams = () => DEFAULT_PARAMS.slice(0)

const createPath = (color, weight, coords) => {
  return {
    key: 'path',
    value: encodeMapParamsComponent([
      {
        key: 'color',
        value: color
      },
      {
        key: 'weight',
        value: weight
      },
      {
        value: coords.from.join(',')
      },
      {
        value: coords.to.join(',')
      }        
    ])
  }
}

const createMarkers = (color, label, coords) => {
  return {
    key: 'markers',
    value: encodeMapParamsComponent([
      {
        key: 'color',
        value: color
      },
      {
        key: 'label',
        value: label
      }  ,
      {
        key: 'size',
        value: 'mid'
      } 
    ].concat(
      coords.map(item => {
        return {
          value: item.join(',')
        }
      })
    ))
  }
}

export default {
  getDefaultParams,
  encodeMapParamsComponent,
  createPath,
  createMarkers
}