import React from 'react';
import ReactDOM from 'react-dom';
import NotFound from '../NotFound';
import '@testing-library/jest-dom/extend-expect';
import { MemoryRouter } from "react-router";

const NotFoundWithRoute = (props) => (
  <MemoryRouter>
    <NotFound {...props} />
  </MemoryRouter>
)

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NotFoundWithRoute />, div);
});