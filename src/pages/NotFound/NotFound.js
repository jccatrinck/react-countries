import React from 'react';
import { Link } from 'react-router-dom';
import styled from '@emotion/styled';
import Page from '../../components/Page/Page';

const StyledDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Pagina404 = () => (
  <Page>
    <StyledDiv>
      <h1>404 - Page not found</h1>
      <br />
      <h3>
        <Link to={`/`}>Go back</Link>
      </h3>
    </StyledDiv>
  </Page>
)

export default Pagina404