import React, { useState, useRef, useCallback, useEffect } from 'react'
import { useQuery } from '@apollo/client';
import styled from '@emotion/styled';
import withApollo from '../../hoc/withApollo';
import { QUERY_COUNTRIES } from '../../gql';
import Card from '../../components/Card/Card';
import Field from '../../components/Field/Field';
import Loading from '../../components/Loading/Loading';
import Page from '../../components/Page/Page';

const StyledDiv = styled.div`
  width: 100%;
  max-width: 600px;
  margin: 0 auto;
`

const Countries = () => {
  const [query, setQuery] = useState('')

  const { loading, error, data, fetchMore }  = useQuery(QUERY_COUNTRIES, {
    variables: { query, limit: 10, offset: 0 },
    notifyOnNetworkStatusChange: true
  })

  const countriesLength = useRef(0)

  useEffect(() => {
    countriesLength.current = 0
  }, [query])

  useEffect(() => {
    if (!loading && data && data.countries) {
      countriesLength.current = data.countries.length
    }
  }, [data, loading])

  let hasMore = true

  if (!loading && data && data.countries) {
    if (countriesLength.current === data.countries.length) {
      hasMore = false      
    }
  }

  const observer = useRef()

  useEffect(() => {
    return () => {
      if (observer.current) {
        observer.current.disconnect()
      }
    }
  }, [])

  const lastCardRef = useCallback(el => {
    if (loading) return
    if (!el) return
    if (observer.current) observer.current.disconnect()
    observer.current = new IntersectionObserver(([entry]) => {
      if (entry.isIntersecting && hasMore) {
        fetchMore({
          variables: {
            offset: data.countries.length
          }          
        })
      }
    }, {
      root: null,
      rootMargin: "0px",
      threshold: 1
    })
    
    observer.current.observe(el)
    
    return () => observer.current.disconnect()
  }, [loading, fetchMore, data, hasMore])

  let cards = null
  
  if (data && data.countries) {
    cards = data.countries.map(({ name, capital, flag: { svgFile: flag } }, index, arr) => {
      let ref;
  
      if (index === arr.length-1) ref = lastCardRef
  
      return (
        <Card key={name} ref={ref} title={name} subtitle={capital} background={flag} link={`/country/${name}`} />
      )
    })
  }

  const handleSearch = (e) => {
    setQuery(e.target.value)
  }

  return (
    <Page>
      <StyledDiv>
        <Field
          label="Search (case sensitive)"
          type="text"
          name="search"
          value={query}
          onChange={handleSearch}
        />
        {cards}
        {error && <p>Error :(</p>}
        {loading && <Loading />}
      </StyledDiv>
    </Page>
  );
}

export default withApollo(Countries)