import React from 'react';
import ReactDOM from 'react-dom';
import Home from '../Home';
import '@testing-library/jest-dom/extend-expect';
import { MockedProvider } from '@apollo/client/testing';
import { QUERY_COUNTRY } from '../../../gql';

const mocks = [
  {
    request: {
      query: QUERY_COUNTRY,
      variables: {
        name: ['Brazil'],
      },
    },
    result: {
      data: {
        countries: [{
          name: 'Brazil',
          capital: 'Brasília',
          flag: {
            svgFile: 'data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg"/>'
          }
        }]
      }
    }
  }
];

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Home />
    </MockedProvider>,
    div
  );
});