import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import withApollo from '../../hoc/withApollo';
import { QUERY_COUNTRY } from '../../gql';
import BackButton from '../../components/BackButton/BackButton';
import Loading from '../../components/Loading/Loading';
import CountryInfo from '../../components/CountryInfo/CountryInfo';
import NearestCountries from '../../components/NearestCountries/NearestCountries';
import Button from '../../components/Button/Button';
import Panel from '../../components/Panel/Panel';
import Page from '../../components/Page/Page';
import Form from '../../components/Form/Form';

const Country = (props) => {
  const { name } = props.match.params

  const [editMode, setEditMode] = useState(false)

  const { loading, error, data }  = useQuery(QUERY_COUNTRY, {
    variables: { name }
  })

  let country;

  if (data && data.countries && data.countries[0]) {
    country = data.countries[0]
  }

  const handleEdit = () => {
    setEditMode(editMode => !editMode)
  }

  let content, editButton;

  if (country) {
    editButton = <Button onClick={handleEdit}>{editMode ? 'Show' : 'Edit'}</Button>

    if (!editMode) {
      const names = country.distanceToOtherCountries.map(item => item.countryName)
  
      content = (
        <Panel>
          <CountryInfo {...country}>
            <NearestCountries origin={country} names={names} />
          </CountryInfo>
        </Panel>
      )
    } else {
      content = (
        <Panel>
          <Form country={country} />
        </Panel>
      )
    }
  }

  return (
    <Page>
      <BackButton />
      {editButton}
      {content}
      {error && <p>Error :(</p>}
      {loading && <Loading />}
    </Page>
  )
}

export default withApollo(Country)
