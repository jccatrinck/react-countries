import { QUERY_COUNTRY } from '../../gql';

export default {
  Mutation: {
    updateCountry: (_, { country: newCountry }, { cache }) => {
      const queryResult = cache.readQuery({
        query: QUERY_COUNTRY,
        variables: {
          name: newCountry.name
        }
      });

      if (queryResult) {
        const { countries: [country] } = queryResult

        newCountry = {
          ...country,
          ...newCountry
        }

        const newCountryRef = cache.writeQuery({
          query: QUERY_COUNTRY,
          data: {
            countries: [newCountry]
          },
        });

        if (newCountryRef) {
          return newCountry;
        }
      }     
      
      return null;
    }
  }
};