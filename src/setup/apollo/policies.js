export const Country = {
  keyFields: ['name'],
  keyArgs: ['filter']
}

export const Query = {
  fields: {
    Country: {
      ...Country,
      merge(existing, incoming, { args: { offset } }) {
        const merged = existing ? existing.slice(0) : [];
        const start = offset ? offset : merged.length;
        const end = start + incoming.length;
        for (let i = start; i < end; ++i) {
          merged[i] = incoming[i - start];
        }
        return merged;
      }
    }
  }
}