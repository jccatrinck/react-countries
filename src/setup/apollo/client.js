import { ApolloClient, InMemoryCache } from '@apollo/client';
import resolvers from './resolvers';
import { Query, Country}  from './policies';
import typeDefs from '../../gql';

const client = new ApolloClient({
  uri: process.env.REACT_APP_APOLLO_URI,
  cache: new InMemoryCache({
    typePolicies: {Query, Country}
  }),
  typeDefs,
  resolvers
});

export default client;
