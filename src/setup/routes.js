import React from 'react';

import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from '../pages/Home/Home';
import Country from '../pages/Country/Country';
import NotFound from '../pages/NotFound/NotFound';

const basename = (process.env.NODE_ENV === 'production' ? process.env.PUBLIC_URL : '/')

export default (
  <BrowserRouter basename={basename}>
    <Switch>
      <Route path="/" component={Home} exact />
      <Route path="/country/:name" component={Country} />
      <Route component={NotFound} />
    </Switch>
  </BrowserRouter>
)