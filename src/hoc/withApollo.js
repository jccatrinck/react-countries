import React from 'react';
import { ApolloProvider } from '@apollo/client';
import client from '../setup/apollo/client';

const withApollo = (WrappedComponent) => (props) => (
  <ApolloProvider client={client}>
    <WrappedComponent {...props} />
  </ApolloProvider>
);

export default withApollo;
