import React from 'react';
import ReactDOM from 'react-dom';
import './config'
import routes from './setup/routes';
import styles from './styles';

ReactDOM.render(
  <>
    {styles}
    <main>{routes}</main>
  </>,
  document.getElementById('⚛️')
);
